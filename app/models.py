from app import db

#Creating a model of datbase used in the web applicaton
#Shows the datatypes that are being used for each fields
#ID is there but never displayed to the user as it is simply a primary key to be able to find unqiue elements in database
# class Task(db.Model):
#     id = db.Column(db.Integer, primary_key = True)
#     db_title = db.Column(db.String(80))
#     db_m_code = db.Column(db.String(120))
#     db_m_desc = db.Column(db.String(120))
#     db_d_date = db.Column(db.String(120))
#     db_complete = db.Column(db.Boolean)


#     def __init__(self, db_title,db_m_code,db_m_desc,db_d_date,db_complete):
#         self.db_title = db_title
#         self.db_m_code = db_m_code
#         self.db_m_desc = db_m_desc
#         self.db_d_date = db_d_date
#         self.db_complete = db_complete


#Models i will need

# Users
class Users(db.Model):
    uId = db.Column(db.Integer, primary_key = True)
    uName = db.Column(db.String(20))
    uEmail = db.Column(db.String(50))
    userPass = db.Column(db.String(100))


    def __init__(self, uName, uEmail, userPass):
        
        self.uName = uName
        self.uEmail = uEmail
        self.userPass = userPass
    
# Reviews

class Reviews(db.Model):
    revId = db.Column(db.Integer, primary_key = True)
    user = db.Column(db.String(50))
    title = db.Column(db.String(50))
    author = db.Column(db.String(50))
    comments = db.Column(db.String(200))
    rating = db.Column(db.Integer)
    reccomend = db.Column(db.Boolean)


    def __init__(self,user,title,author,comments,rating,reccomend):
        self.user = user
        self.title = title
        self.author = author
        self.comments = comments
        self.rating = rating
        self.reccomend = reccomend
##