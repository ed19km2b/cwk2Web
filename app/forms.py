from flask_wtf import FlaskForm
from wtforms import (IntegerField,DateField,
    PasswordField,
    SelectField,
    StringField,
    SubmitField,
    TextAreaField,
    RadioField)
from wtforms.validators import URL, DataRequired, Email, EqualTo, Length


class CalculatorForm(FlaskForm):
    number1 = IntegerField('number1', validators=[DataRequired()])
    number2 = IntegerField('number2', validators=[DataRequired()])


class ReviewForm(FlaskForm):
    title = StringField("Title", validators=[DataRequired()])
    review = IntegerField("Score out of 10", validators=[DataRequired()])

class LoginF(FlaskForm):
    userN = StringField("Username:", validators=[DataRequired(), Length(max=20)])
    passW = StringField("Password:", validators=[DataRequired()])
    submitB = SubmitField("Submit")


class Register(FlaskForm):
    userN = StringField("Choose Unique Username:", validators=[DataRequired(), Length(max=20)])
    passW1 = StringField("Enter Password:", validators=[DataRequired()])
    passW2 = StringField("Confirm Password:", validators=[DataRequired()])
    uemail = StringField("Enter Email:", validators=[DataRequired(), Length(max=50)])
    submitB = SubmitField("Submit")

class changePass(FlaskForm):
    currPass = StringField("Enter Current Password", validators=[DataRequired()])
    newPass1 = StringField("Enter New Password", validators=[DataRequired()])
    newPass2 = StringField("Confirm New Password", validators=[DataRequired()])
    submitB = SubmitField("Submit")



class revBook(FlaskForm):
    title = StringField("Enter title of book", validators=[DataRequired(), Length(max=50)])
    author = StringField("Enter author of book",validators=[DataRequired(), Length(max=50)])
    rating = IntegerField("Did you enjoy the book. 1-10",validators=[DataRequired()])
    comments = StringField("Any thoughts on the book?", validators=[DataRequired(), Length(max=200)])
    reccomend = RadioField("Would you reccomend the book to someone else:", choices=[(True,'Yes'),(False,'No')], validators=[DataRequired()])
    submitB = SubmitField("Submit")



"""
https://github.com/hackersandslackers/flask-wtform-tutorial/blob/master/flask_wtforms_tutorial/forms.py
https://hackersandslackers.com/flask-wtforms-forms/

"""