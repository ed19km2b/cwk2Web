from app import app
from flask import render_template, flash, request, url_for, Flask, redirect, session
from .forms import LoginF, Register, changePass, revBook
from flask_sqlalchemy import SQLAlchemy
from app import app
from app import models
from app import db
import datetime
import logging
from werkzeug.security import generate_password_hash, check_password_hash



@app.route('/', methods=['GET', 'POST'])
def view_uncompleted():


    userN = None
    form = LoginF()

    if session.get('user'):
        logging.error("User attempted to sign in whilst already signed in - LOG IN")
        return redirect(url_for("view_all"))
    
    if form.validate_on_submit() and request.method == 'POST': # if all the data fields are filled in
        name = form.userN.data
        pass1 = form.passW.data
        form.passW.data = ''
        foundUser = models.Users.query.filter_by(uName = name).first()
        if foundUser != None:
            checkpass = check_password_hash(foundUser.userPass, pass1) # compare hashed password ind atabase with user entered password when logging in.
            if checkpass:
                #user was found
                session['user'] = name
                return redirect(url_for("view_all"))
            else:
                flash("ERROR: Username and/or password entered was incorrect. Please try again")
        else:
            flash("ERROR: Username and/or password entered was incorrect. Please try again")
                
                
        # flash("ERROR: Username and/or password entered was incorrect. Please try again")
        return render_template("view_uncompleted.html", form=form)

    else:
        logging.warning('User Failed attempted login')
        return render_template("view_uncompleted.html", form=form)

    

@app.route('/changeP', methods=['GET','POST'])
def changeP():
    if not session.get('user'):
        logging.error("Unathorised access to page - PASSWORD CHANGE")
        return redirect(url_for("view_uncompleted"))
    form = changePass()
    if form.validate_on_submit():
        char_p = False
        num_p = False
        pass1 = form.currPass.data
        np1 = form.newPass1.data
        np2 = form.newPass2.data
        foundUser = models.Users.query.filter_by(uName = session['user']).first()
        if foundUser != None:
            checkpass = check_password_hash(foundUser.userPass, pass1) # compare hashed password ind atabase with user entered password when logging in.
            if not checkpass:
                flash("ERROR: Incorrect Current Password Entered")
                form.newPass1.data=""
                form.newPass2.data=""
                logging.warning("ERROR: User Failed to Change password")
            else:
                if np1 != np2:
                    flash("ERROR: Newly selected passwords entered do not match")
                    form.newPass1.data=""
                    form.newPass2.data=""
                    logging.warning("ERROR: User Failed to Change password")
                else:
                    if not (8<=len(np1)<=20):
                        flash("ERROR: New password must contain between 8-20 characters")
                        form.newPass1.data=""
                        form.newPass2.data=""
                        logging.warning("ERROR: User Failed to Change password")
                    else:
                        for elem in np1:
                            if elem.isnumeric():
                                num_p = True
                            elif elem.isalpha():
                                char_p = True
                            
                        if not (num_p and char_p):
                            flash("ERROR: Password must contain a mixture of numbers and letters")
                            form.newPass1.data=""
                            form.newPass2.data=""
                            logging.warning("ERROR: User Failed to Change password")
                        else:

                            hashpass = generate_password_hash(np1, method='sha256')
                            foundUser.userPass = hashpass
                            db.session.commit()
                            return render_template("view_all.html", temp=session['user'])
                        return render_template("changeP.html", form=form)
                    return render_template("changeP.html", form=form)
                return render_template("changeP.html", form=form)
            return render_template("changeP.html", form=form)
        return render_template("changeP.html", form=form)
    return render_template("changeP.html", form=form)


@app.route('/delete/<int:revId>')
def deleteRev(revId):
    if not session.get('user'):
        logging.error("Unauthorised access to route - DELETE")
        return redirect(url_for("view_uncompleted"))
    review_to_delete = models.Reviews.query.filter_by(revId=revId).first()
    db.session.delete(review_to_delete)
    db.session.commit()
    return redirect(url_for("view_all"))
    

@app.route('/add', methods=['GET', 'POST'])
def addTask():
    form = Register()
    if session.get('user'):
        logging.error("User attempted to register whilst signed in - REGISTER")
        return redirect(url_for("view_all"))
    if request.method == "POST" and form.validate_on_submit():
        char_p = False
        num_p = False
        req_user = form.userN.data
        uPass1 = form.passW1.data
        uPass2 = form.passW2.data
        req_Email = form.uemail.data
        user_check = models.Users.query.filter_by(uName = req_user).first()
        if user_check != None:
            flash("Error. Username associated to account")
            form.passW1.data=""
            form.passW2.data=""
            logging.warning("ERROR: Registration Failed")
        else: # check if the username exists in the database
            if '@' in req_Email: # error checking for email - for now just looks for @
                email_check = models.Users.query.filter_by(uEmail= req_Email).first()
                if email_check != None:
                    flash("Error. Email associated to account")
                    form.passW1.data=""
                    form.passW2.data=""
                    logging.warning("ERROR: Registration Failed")
                else:
                    if uPass1 != uPass2:
                        flash("Error. Passwords don't match.")
                        form.passW1.data=""
                        form.passW2.data="" # check if user entered passwords match
                        logging.warning("ERROR: Registration Failed")
                    else:
                        if not (8<=len(uPass1)<=20):
                            flash("Error. Password must be of length 8-20 characters")
                            form.passW1.data=""
                            form.passW2.data=""
                            logging.warning("ERROR: Registration Failed")
                        else:
                            for elem in uPass1:
                                if elem.isnumeric():
                                    num_p = True
                                elif elem.isalpha():
                                    char_p = True
                            
                            if not (num_p and char_p):
                                flash("ERROR: Password must contain a mixture of numbers and letters")
                                form.passW1.data=""
                                form.passW2.data=""
                                logging.warning("ERROR: Registration Failed")
                            else:


                                #hashpass - basic password encryption to not save passwords as plaintext
                                hashpass = generate_password_hash(uPass1, method='sha256') 
                                user = models.Users(uName = req_user, uEmail=req_Email, userPass = hashpass)
                                db.session.add(user)
                                db.session.commit()
                                session['user'] = req_user

                                return redirect(url_for("view_all"))
                            return render_template("add.html", form=form)
                        return render_template("add.html", form=form)
                    return render_template("add.html", form=form)
                return render_template("add.html", form=form)
            return render_template("add.html", form=form)
        return render_template("add.html", form=form)
    return render_template("add.html", form=form)




@app.route('/completed', methods=['GET','POST'])
def view_completed():
    if not session.get('user'):
        logging.error("Unauthorised access to page - REVIEW BOOK")
        return redirect(url_for("view_uncompleted"))
    form=revBook()
    if form.validate_on_submit() and request.method == "POST":
        title=form.title.data
        author=form.author.data
        rating=int(form.rating.data)
        comments=form.comments.data
        reccomend=form.reccomend.data

        if reccomend == "True":
            reccomend = True
        else:
            reccomend =False

        if not (1<=rating<=10):
            flash("ERROR - Rating must be between 1 and 10")
            logging.warning("ERROR - user failed to add review")
        
        else:

            review = models.Reviews(user=session['user'],title=title,author=author,comments=comments,rating=rating, reccomend=reccomend)
            db.session.add(review)
            db.session.commit()


            return redirect(url_for("view_all"))
        return render_template("view_completed.html", form=form)
    else: 
        return render_template("view_completed.html", form=form)


    # return render_template("view_all.html", temp=session['user'])


@app.route('/uncompleted', methods=['GET', 'POST'])
def view_all():
    if not session.get('user'):
        logging.error("Unauthorised access to page - VIEW REVIEWS")
        return redirect(url_for("view_uncompleted"))
    user_revs = models.Reviews.query.filter_by(user = session['user']).all()
    
    return render_template("view_all.html", temp=session['user'],revs=user_revs)
    

@app.route('/logout')
def logout():
    if not session.get('user'):
        logging.error("Unauthorised access to page - LOGOUT")
        return redirect(url_for("view_uncompleted"))
    session.pop('user', None) # removes user key from session, effictively logs them out


    

    return (redirect(url_for("view_uncompleted")))
if __name__ == "__main__":
    # db.create_all()
    app.run(debug=True)
